package sortersTesterClasses;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

import sorterClasses.BubbleSortSorter;
import sorterClasses.InsertionSortSorter;
import sorterClasses.SelectionSortSorter;
import interfaces.Sorter;

public class IntegerSorterTester {
	//private static Sorter<Integer> sorter; 
	private static Random rnd; 
	private static ArrayList<Sorter<Integer>> sortersList = new ArrayList<>(); 
	
	public static void main(String[] args) { 
		sortersList.add(new BubbleSortSorter<Integer>()); 
		sortersList.add(new SelectionSortSorter<Integer>()); 
		sortersList.add(new InsertionSortSorter<Integer>()); 
		
		test("Sorting Using Default Comparator<Integer>", null); 
		test("Sorting Using IntegerComparator1", new IntegerComparator1()); 
		test("Sorting Using IntegerComparator2", new IntegerComparator2()); 
		testEntero("Sorting Using Entero"); // Exercise 1 tester method
		testComparators("Sorting Using Two Comparators"); // Exercise 2 method
	}
	
	private static void test(String msg, Comparator<Integer> cmp) { 
		rnd = new Random(101); 

		System.out.println("\n\n*******************************************************");
		System.out.println("*** " + msg + "  ***");
		System.out.println("*******************************************************");
		
		Integer[] original, arr; 
		// generate random arrays is size i and test...
		for (int i=1; i<=20; i += 5) { 
			original = randomValues(i);
			showArray("\n ---Original array of size " + i + " to sort:", original); 
			
			for (int s=0; s<sortersList.size(); s++) {
				Sorter<Integer> sorter = sortersList.get(s); 
			    arr = original.clone(); 
			    sorter.sort(arr, cmp);
			    showArray(sorter.getName() + ": ", arr); 
			}
		}
	}
	
	// Exercise 1 tester method
	private static void testEntero(String msg) {
		rnd = new Random(101);
			
		System.out.println("\n\n*******************************************************");
		System.out.println("*** " + msg + "  ***");
		System.out.println("*******************************************************");
			
		Integer[] original;
		Entero[] enteroArr;
			
		original = randomValues(20);
		enteroArr = new Entero[original.length];
				
		showArray("\n ---Original array of size " + 20 + " to sort:", original); 
				
		for (int j = 0; j < original.length; j++) {
			enteroArr[j] = new Entero(original[j]);
		}
				
		Sorter<Entero> sorter = new InsertionSortSorter<Entero>(); 	
		sorter.sort(enteroArr, null);
				
		System.out.print(sorter.getName() + " / Entero : "); 
				
		for (int z = 0; z < enteroArr.length; z++) {
			System.out.print("\t" + enteroArr[z].getValue()); 
		}
				
		System.out.println();
	}

	// Exercise 2 method
	private static void testComparators(String msg) {
		rnd = new Random(101); 

		System.out.println("\n\n*******************************************************");
		System.out.println("*** " + msg + "  ***");
		System.out.println("*******************************************************");
			
		Integer[] arr = {5, 9, 20, 22, 20, 5, 4, 13, 17, 8, 22, 1, 3, 7, 11, 9, 10};
			
		Sorter<Integer> sorter = sortersList.get(0); // Bubble Sort
			
		showArray("\n ---Original array of size " + arr.length + " to sort:", arr);
		System.out.println();
			
		Integer[] copy = arr.clone();
			
		sorter.sort(copy, new IntegerComparator1());
		showArray(sorter.getName() + " / First Comparator: ", copy);
			
		sorter.sort(arr, new IntegerComparator2());
		showArray(sorter.getName() + " / Second Comparator: ", arr);
	}


	private static void showArray(String msg, Integer[] a) {
		System.out.print(msg); 
		for (int i=0; i<a.length; i++) 
			System.out.print("\t" + a[i]); 
		System.out.println();
	}

	private static Integer[] randomValues(int i) {
		Integer[] a = new Integer[i]; 
		for (int j=0; j<i; j++) 
			a[j] = rnd.nextInt(100); 
		return a;
	}

}
